﻿using UnityEngine;
using System.Collections;

public class PowerUps : MonoBehaviour
{
    public GameObject PowerUpPrefab;
    public int PowerUpCount = 3;
    public float PowerUpRadius = 1;

    /// <summary>
    /// Spawn a circle of PowerUpCount power up prefabs stored in PowerUpPrefab, evenly spaced, around the player with a radius of PowerUpRadius
    /// </summary>
    /// <returns>An array of the spawned power ups, in counter clockwise order.</returns>
    public GameObject[] SpawnPowerUps()
    {
        GameObject[] powerups = new GameObject[PowerUpCount];
        float angle = (2 * Mathf.PI) / PowerUpCount;

        for (int i = 0; i < PowerUpCount; i++) {
             float new_angle = angle * i  ;//Udating angle

            Vector3 point_in_a_circle = new Vector3(PowerUpRadius * Mathf.Cos(new_angle), PowerUpRadius * Mathf.Sin(new_angle), 0);
            Vector3 location = transform.position + point_in_a_circle;

            powerups[i] = Instantiate(PowerUpPrefab, location, Quaternion.identity);
        }

        return powerups;
    }
}
