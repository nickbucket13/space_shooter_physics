﻿using UnityEngine;
using System.Collections;

public class BombLine : MonoBehaviour
{

    public GameObject BombPrefab;
    public int BombCount;
    public float BombSpacing;

    /// <summary>
    /// Spawn a line of instantiated BombPrefabs behind the player ship. There should be BombCount bombs placed with BombSpacing amount of space between them.
    /// </summary>
    /// <returns>An array containing all the bomb objects</returns>
    public GameObject[] SpawnBombs()
    {
        GameObject[] bombs = new GameObject[BombCount];

        for (int i = 0; i < BombCount; i++) {
            Vector3 back_of_ship_with_spacing = -transform.up * ((i + 1) * BombSpacing);
            bombs[i] = Instantiate(BombPrefab, transform.position + back_of_ship_with_spacing, Quaternion.identity);
        }
        return bombs;
    }
    
}
