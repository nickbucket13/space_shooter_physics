﻿using UnityEngine;
using System.Collections;

public class BombSpiral : MonoBehaviour
{
    public GameObject BombPrefab;
    [Range(5, 25)]
    public float SpiralAngleInDegrees = 10;
    public int BombCount = 10;
    public float StartRadius = 1;
    public float EndRadius = 3;

    /// <summary>
    /// Spawns spirals of BombPrefab game objects around the player. Create BombCount number of bombs 
    /// around the player, with each bomb being spaced SpiralAngleInDegrees apart from the next. The spiral 
    /// starts at StartRadius away from the player and ends at EndRadius away from the player.
    /// </summary>
    /// <returns>An array of the spawned bombs</returns>
    public GameObject[] SpawnBombSpiral()
    {
        GameObject[] bombs = new GameObject[BombCount];

        float angle_in_rads = SpiralAngleInDegrees * Mathf.Deg2Rad;
        //Base case to avoid 0/0
        if (BombCount == 1) {
            Vector3 point_in_a_circle = new Vector3(StartRadius * Mathf.Cos(angle_in_rads), StartRadius * Mathf.Sin(angle_in_rads), 0);
            Vector3 location = transform.position + point_in_a_circle;

            bombs[0] = Instantiate(BombPrefab, location, Quaternion.identity);
            return bombs;
        }

        for (int i = 0; i < BombCount; i++)
        {
            float updated_angle = angle_in_rads * i;   
            //Subtract one from the total numberof bombs in order to reach the end radius
            float radius = Mathf.Lerp(StartRadius,EndRadius, (float)i / (float)(BombCount - 1));
            Vector3 point_in_a_circle = new Vector3( Mathf.Cos(updated_angle),  Mathf.Sin(updated_angle), 0) * radius;
            Vector3 location = transform.position + point_in_a_circle;
            //Debug.Log(radius);
            bombs[i] = Instantiate(BombPrefab, location, Quaternion.identity);

        }

        return bombs;
    }
}


