﻿using UnityEngine;
using System.Collections;

public class VectorToEnemy : MonoBehaviour
{
    
    /// <summary>
    /// Calculated vector from the player to enemy found by GameManager.GetEnemyObject
    /// </summary>
    /// <see cref="GameController.GetEnemyObject"/>
    /// <returns>The vector from the player to the enemy.</returns>
    public Vector3 GetVectorToEnemy()
    {
        Vector3 enemy_position = GameController.GetEnemyObject().transform.position;
        //Debug.Log(enemy_position - transform.position);
        return enemy_position - transform.position;
    }

    /// <summary>
    /// Calculates the distance from the player to the enemy returned by GameManager.GetEnemyObject without using calls to magnitude.
    /// </summary>
    /// <see cref="GameController.GetEnemyObject"/>
    /// <returns>The scalar distance between the player and the enemy</returns>
    public float GetDistanceToEnemy()
    {
        Vector3 vec_to_enemy = GameController.GetEnemyObject().transform.position - transform.position;
        float sx = vec_to_enemy.x * vec_to_enemy.x;
        float sy = vec_to_enemy.y * vec_to_enemy.y;
        //Debug.Log(Mathf.Sqrt(sx + sy));
        return Mathf.Sqrt(sx + sy);
    }
    
}
