﻿using UnityEngine;
using System.Collections;

public class VisionCone : MonoBehaviour
{

    public float AngleSweepInDegrees = 60;
    public float ViewDistance = 3;

    /// <summary>
    /// Calculates whether the player is inside the vision cone of an enemy as defined by the AngleSweepIndegrees
    /// and ViewDistance varilables. Do not use any magnitude or Distance methods.  You may use any of the previous
    /// methods you have written.
    /// </summary>
    /// <see cref="GameController"/>
    /// <returns>Whether the player is within the enemy's vision cone.</returns>

    
    
    public bool IsPlayerInVisionCone()
    {
        bool is_player_in_visionCone = false;

        Vector3 vector_to_player = GameController.GetPlayerObject().GetComponent<VectorToEnemy>().GetVectorToEnemy() * -1f;
        float angle_of_rotation = Mathf.Atan2(transform.up.y, transform.up.x);
        float angle_in_rads = (AngleSweepInDegrees * Mathf.Deg2Rad) / 2;

        float angle_to_player = Mathf.Atan2(vector_to_player.y, vector_to_player.x);
        float distance_to_player = GameController.GetPlayerObject().GetComponent<VectorToEnemy>().GetDistanceToEnemy();

        if (distance_to_player <= ViewDistance && angle_to_player <= angle_of_rotation + angle_in_rads && angle_to_player >= angle_of_rotation - angle_in_rads) { is_player_in_visionCone = true; }
        

        return is_player_in_visionCone;
    }
    
}
