﻿using UnityEngine;
using System.Collections;

public class ShipMotor : MonoBehaviour
{
    public float AccelerationTime = 1;
    public float DecelerationTime = 1;
    public float MaxSpeed = 1;

    //Track current speed and latest direction
    [SerializeField]
    private float current_speed;
    private Vector2 latest_direction;

    /// <summary>
    /// Move the ship using it's transform only based on the current input vector. Do not use rigid bodies.
    /// </summary>
    /// <param name="input">The input from the player. The possible range of values for x and y are from -1 to 1.</param>
    public void HandleMovementInput( Vector2 input )
    {
        //acceleration is velocity/time --> we need to know what that will be per frame
        float acceleration = (MaxSpeed / AccelerationTime) * Time.deltaTime;
        float deceleration = (MaxSpeed / DecelerationTime) * Time.deltaTime;

        if (Mathf.Abs(input.x) > 0.5f || Mathf.Abs(input.y) > 0.5f) // If there is any input at all
        {
            current_speed += acceleration; //Linear velocity increase from a constant acceleration

            latest_direction = input; //Update direction
        }
        else
        {
            current_speed -= deceleration; //Linear velocity decrease from a constant deceleration
        }

        current_speed = Mathf.Clamp(current_speed, 0, MaxSpeed); //Velocity must never be less than 0 or greater than max speed
        Vector3 world_space_input = GameController.GetCamera().transform.TransformDirection(latest_direction);
        transform.Translate(world_space_input * current_speed * Time.deltaTime, Space.World);//Move ship based on input and current acceleration 
    }
    
}
