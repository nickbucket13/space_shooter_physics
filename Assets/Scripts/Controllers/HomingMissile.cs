﻿using UnityEngine;
using System.Collections;

public class HomingMissile : MonoBehaviour
{
    public float ForwardSpeed = 1;
    public float RotateSpeedInDeg = 45;

    // In Update, you should rotate and move the missile to rotate it towards the player.  It should move forward with ForwardSpeed and rotate at RotateSpeedInDeg.
    // Do not use the RotateTowards or LookAt methods.
    void Update()
    {
        //Get vector to enemy
        Vector3 vector_to_enemy_position = GameController.GetEnemyObject().transform.position - transform.position;
        //Get the angle between the missile and the vector to th enemy
        float angle_to_enemy = Vector3.Angle(vector_to_enemy_position, transform.up);
        float dot_of_positions = Vector3.Dot(vector_to_enemy_position,transform.right);
        //Check the signage of the dot product
        float signage = -Mathf.Sign(dot_of_positions);

        if (angle_to_enemy != 0) {
            //update the right angle it is supposed to rotate to
            float update_angle = angle_to_enemy;
            if (angle_to_enemy == 180) {//Base case
                update_angle = Mathf.Clamp(update_angle, 0f, RotateSpeedInDeg);
                transform.Rotate(0, 0, update_angle * Time.deltaTime, Space.Self);
            }
            update_angle = Mathf.Clamp(update_angle, 0f, RotateSpeedInDeg);
            //rotate object
            transform.Rotate(0, 0, update_angle * signage * Time.deltaTime, Space.Self);
        }

        //Missile will always be moving foward
        transform.Translate((Vector3.up * Time.deltaTime) * ForwardSpeed);
    }
}
